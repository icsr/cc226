function main()
    import java.util.*;
  
    datasetTitle = 'bodyfat';
    xIndexes = [7 5]; % utilizando addomen e neck
    yIndex = 15;      % percentual de gordura que ser� estimado  
    qtyOfGroups = 10; % 10 cross-fold
    
    close;close;
    
    format long;
    samples = loadSamples(datasetTitle);    
    groups = separateGroups(samples, xIndexes, yIndex, qtyOfGroups);
    ks = groups.keySet();
    it = ks.iterator();
    i = 1;
    
    while(it.hasNext())
        tgk = it.next();
        testGroup = groups.get(tgk);
                
        trainingGroup = getSamplesGroup(groups, tgk);
        [X, Y, estimatedY, errRMS] = linearRegression(trainingGroup, testGroup, xIndexes, yIndex);
        
        errorsRMS(i) = errRMS;
        i = i+1;
    end
    
    plotError(errorsRMS);
    
    labels(1, :) = 'Percent body fat          ';      
    labels(2, :) = 'Abdomen circumference (cm)';    
    labels(3, :) = 'Neck circumference (cm)   ';
    plotRegressionCurves(samples(:, xIndexes), samples(:, yIndex), 10000, labels);

end

function samplesGroup = getSamplesGroup(groups, testGroupKey)

    ks = groups.keySet();
    it = ks.iterator();
    i = 0;
        
    while(it.hasNext)
       k = it.next();
       if(k == testGroupKey)
           continue;
       end
       sg = groups.get(k);
       for(j = 1 : length(sg))
            i = i + 1;
            samplesGroup(i, :) = sg(j, :);
       end

    end        

end

function samples = loadSamples(datasetTitle)

    clear samples;
    matFile = char([datasetTitle, '.mat']);
    arffFile = char([datasetTitle, '.arff']);

    if(~exist('samples'))
       if(exist(matFile))
           load(matFile);
       else if(exist(arffFile))
               samples = arff2Mat(arffFile);
           else
               error('Nenhum arquivo .mat ou .arff encontrado.');
           end
       end
    end
end

function groups = separateGroups(samples, xIndexes, yIndex, qtyOfGroups)
    groups = java.util.HashMap;
    
    n = round(length(samples)/qtyOfGroups);
    
    for(i = 1 : qtyOfGroups)
        
        final = n * i;
        if(i == qtyOfGroups)
            final = length(samples);
        end
                
        groups.put(i, samples( n * (i-1) + 1 : final , :));
                
    end
    
end
