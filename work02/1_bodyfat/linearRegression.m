function [X2, Y2, estimatedY2, errRMS, B] = linearRegression(trainingGroup, testGroup, xIndexes, yIndex)
    
    X1 = trainingGroup(:, xIndexes);
    Y1 = trainingGroup(:, yIndex);
    
    X2 = testGroup(:, xIndexes);
    Y2 = testGroup(:, yIndex);
    
    B = inv(X1'*X1) * X1'*Y1;
    
    estimatedY2 = X2 * B;
    errRMS = calcErrRMS(estimatedY2, Y2, B);
    
end
