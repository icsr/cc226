function plotRegressionCurves(x, y, pointsQty, labels)

    figure('units', 'normalized', 'outerposition', [0.5 0 0.5 1]);
    
    [lines columns] = size(x);
   
    samples = [x y];
    [X, Y, estimatedY, errRMS, B] = linearRegression(samples, samples, 1 : columns, columns + 1);
    regSamples = [X estimatedY];
    
     for(i = 1 : columns)
        minX(i) = min(x(:, i));
        maxX(i) = max(x(:, i));
        
        delta = (maxX(i) - minX(i)) / pointsQty;
        c = (y(length(y)) - y(1))/(x(length(x), i) - x(1, i));
        
        if(c > 0)
            eX(:, i) = minX(i) : delta : maxX(i);
        else
            eX(:, i) = maxX(i) : -delta : minX(i);
        end       
        
     end
    
     eY = eX * B;    
     fitSamples = [eX eY];
        
    for(i = 1 : columns)
        subplot(columns, 1, i);
        hold on;
        
        regSamples = sortrows(regSamples, i);        
        plot(regSamples(:, i), regSamples(:, columns+1), 'ro');
                
        plot(samples(:, i), samples(:, columns+1), 'bo');
        plot(fitSamples(:, i), fitSamples(:, columns+1), 'g-');
        
     
        xlabel(labels(i+1, :));
        ylabel(labels(1, :));
        legend('Estimado', 'Verdadeiro', 'Fit Curve');
        hold off;
    end
    
    subtitle(char(['Regress�o com Erro RMS de ' num2str(errRMS)]));
            
end

function [ax,h]=subtitle(text)
    ax=axes('Units','Normal','Position',[.075 .075 .85 .85],'Visible','off');
    set(get(ax,'Title'),'Visible','on');
    title(text);
    if (nargout >= 2)
        h=get(ax,'Title');
    end    
end