function plotError(errorsRMS)

    figure('units', 'normalized', 'outerposition', [0 0 0.5 1]);
    
    x = 1 : length(errorsRMS);
    bar(x, errorsRMS);
    grid on
    title('Avalia��o de bodyfat com Regress�o Linear M�ltipla');
    xlabel('10-fold cross-validation');  
    ylabel('Erro RMS');

end