function f = gaussianFunction(x1, x2, x1c, x2c, bw)    
    f = (x1 - x1c).^2 + (x2 - x2c).^2;
    f = exp(-f/2/bw);
end