function clusters = findClusters(X, Y, Z, R)
    import java.util.*;
    clusters = java.util.LinkedList;
   
    while(true)            
        if(isempty(X))
            break;
        end
        
        removeIndexes = [];
        cluster = [];
        k = 1;

        x0 = X(1);
        y0 = Y(1);
                
        for(j = 1 : length(X))        
            x1 = X(j);
            y1 = Y(j);

            if(sqrt((x1-x0)^2 + (y1-y0)^2) <= R)
                cluster(k, :) = [X(j) Y(j) Z(j)];
                removeIndexes = [removeIndexes j];
                k = k + 1;
            end
        end
        
        X(removeIndexes,:) = [];
        Y(removeIndexes) = [];
        Z(removeIndexes) = [];
        if(~isempty(cluster))
            clusters.add(cluster);
        end
    end

end