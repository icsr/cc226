function plotHeat(X,Y,Z, labels, x, y, z)
    figure('units', 'normalized', 'outerposition', [0 0 0.7 0.7]);
    hold on
    
    [X, Y] = switchXY(X,Y, Z);
    [x, y] = switchXY(x,y, z);   
 
    h(1) = surf(X,Y,Z, 'EdgeColor', 'none', 'FaceAlpha', 0.8);
    view(2);
    
    colormap(jetWhite);
    c = colorbar;
    
    xtick = [min(X) : 1 : max(X)];
    ytick = [min(Y) : 1 : max(Y)];
        
    grid minor    
     
    title(labels(1));
    xlabel(labels(2));
    ylabel(labels(3));
    xlabel(c, labels(4));    
      
    
    h(2) = plot3(x, y, z, 'ro');            
    set(h(2),'Visible','off');
    plotGoogleMap('MapType', 'hybrid', 'Alpha', 0.7);
    
    
    colormap(jetWhite);
    alphamap('rampdown')
    alphamap('increase',.1)
    c = colormap;
     
end

function [Y X] = switchXY(X,Y, Z)
    
    

end

