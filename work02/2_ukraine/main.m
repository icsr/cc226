function main()    
        
    datasetTitle = 'ukraine';
    radiusOfNeighboors = 0.1;        
    gridWidth = 500;
    gridHeight = gridWidth;
    bandwidth = 0.1;
    cutoff = 10;
    
    load([datasetTitle, '.mat']);
    format long;
    close;
    close;
      
    X = samples(:,1);
    Y = samples(:,2);
    Z = samples(:,3);
    
    labels = {'An�lise do Conflito de Donetsk', 'Longitute (graus)', 'Latitude (graus)' ,'Fatalidades'};
    
    heatyMap(X, Y, Z, cutoff, radiusOfNeighboors, gridWidth, gridHeight, bandwidth, labels);

end