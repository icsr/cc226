function heatyMap(X, Y, Z, cutoff, radiusOfNeighboors, gridWidth, gridHeight, bandwidth, labels)   

    m = gridWidth;
    n = gridHeight;
    R = radiusOfNeighboors;
    bw = bandwidth;    

    clusters = findClusters(X,Y,Z, R);
    it = clusters.iterator();
        
    minX = min(X);
    maxX = max(X);
    minY = min(Y);
    maxY = max(X);
    
    xi = linspace(minX, maxX, n);
    yi = linspace(minY, maxY, n);
    zi = zeros(n);    
    maxZ = 0;
       
    while(it.hasNext)        
       cluster = column2line(it.next);  
       
       x = mean(cluster(:, 1));
       y = mean(cluster(:, 2));
       z = sum(cluster(:, 3));

       maxZ = max(maxZ, z);
       
       for(i = 1 : length(xi))
           for(j = 1 : length(yi))
                zi(i,j) = zi(i,j) + z*gaussianFunction(xi(i), yi(j), x, y, bw);               
           end
       end    
       
    end
    
     for(i = 1 : length(xi))
           for(j = 1 : length(yi))
                if(zi(i,j) < cutoff)
                    zi(i,j) = NaN;
                end
           end
     end
            
    zi = zi * maxZ / max(max(zi));
    plotHeat(xi, yi, zi, labels, X, Y, Z);
    
end

function data = column2line(data)
    if(iscolumn(data))
       data = data'; 
    end
end