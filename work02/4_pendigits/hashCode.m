function h = hashCode(string)
    import java.lang.String;
    
    s = java.lang.String(string);
    h = s.hashCode();

end