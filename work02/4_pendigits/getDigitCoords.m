function coord = getDigitCoords(samples, line)
    for(i=1:8)
            coord(i,1)= samples(line, 2*i - 1);
            coord(i,2)= samples(line, 2*i);
    end
end