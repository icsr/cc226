function screePlot(label, eValues)

	figure('units', 'normalized', 'outerposition', [0.5 0 0.5 1]);
    y = sort(eValues, 'descend');
    x = 1:length(y);
    plot(x', y', 'o-');
    xlabel('Ordem do Autovalor');
    ylabel('Autovalor');
    grid on;
    title(label);
    
end