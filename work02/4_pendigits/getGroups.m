function groups = getGroups(samples, classColumn)
    clear groups data;
    import java.util.HashMap; 
    
    groups = java.util.HashMap;
    classes = samples(:,classColumn);
    data = samples;
    data(:, classColumn) = [];

    distClasses = sort(unique(classes));
    
    for(d = 1 : length(distClasses))
        clear coords i ;
        i = find(classes==distClasses(d));           
        groups.put(distClasses(d), data(i,:));
    end   
end