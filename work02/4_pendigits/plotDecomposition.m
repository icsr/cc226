function plotDecomposition(samples, E, reducedParamsQty)

    

    for(i = 1 : length(samples))
        X = samples(i, 1:16);
        L(i, :) = X * E' * inv(E * E');
        C(i) = samples(i, 17);        
    end
    
    scatter3(L(:,1), L(:,2), C, 30, C)
    view(2)
    grid on
    
    
    map =   [1.0 0.0 0.0; 
             1.0 0.0 1.0; 
             1.0 1.0 0.0; 
             1.0 0.5 0.5; 
             0.0 1.0 0.0; 
             0.0 0.0 1.0; 
             0.5 0.5 1.0; 
             0.7 0.8 1.0; 
             0.5 0.8 1.0;
             0.0 0.0 0.0;
             ];
             
    colormap(map);
    c = colorbar;
    xlabel('Coeficiente do Autovetor de maior ordem');
    ylabel('Coeficiente do Segundo Autovetor de maior ordem');
    title('Combina��o Linear dos Autovetores');
    
    xlabel(c, 'D�gito da amostra');

end