function plotDigit(samples, line)
    coord = getDigitCoords(samples, line);
    figure
    plot(coord(:,1), coord(:,2),'-');
end