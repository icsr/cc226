function [Sb Sw overallMean]= scatterMatrix(samples, classColumn)
    import java.util.*;

    g = getGroups(samples, classColumn);
        
    data = samples;
    data(:, classColumn) = [];
    
    [dataSize, attributesSize] = size(data); 
            
    Sb=zeros(attributesSize);        
    Sw=zeros(attributesSize);  
    
    overallMean=mean(data);
    
    ks = g.keySet();
    it = ks.iterator();
    while(it.hasNext())
        d = it.next;
        
        perClassData=g.get(d);        
        perClassMean = mean(perClassData);
        N = length(perClassData);
         
        perClassData=perClassData - ones(N, 1) * perClassMean;
        Sw = Sw + perClassData' * perClassData;     
        
        deltaMean = perClassMean - overallMean;        
       
        Sb = Sb + N * deltaMean' * deltaMean;
    end
end
