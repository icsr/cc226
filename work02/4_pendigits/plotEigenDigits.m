function [reducedData, eVector, eValues, E] = plotEigenDigits(label, samples, classColumn, reducedParamsQty)

    [reducedData, eVector, eValues, overallMean] = flda(samples, classColumn, reducedParamsQty);

    E = eVector' + repmat(overallMean', 1, length(eVector));
    n = length(E);
    figure('units', 'normalized', 'outerposition', [0 0 0.5 1]);
                
    for(i = 1 : reducedParamsQty)        
         ph(i) = subplot(1, 2, i);
         coord = getDigitCoords(E, i);
         plot(coord(:,1), coord(:,2),'-');
    end
    
    subtitle(label);
      
end

function [ax,h]=subtitle(text)
    ax=axes('Units','Normal','Position',[.075 .085 .85 .85],'Visible','off');
    set(get(ax,'Title'),'Visible','on');
    title(text);
    if (nargout >= 2)
        h=get(ax,'Title');
    end    
end