function main()
    datasetTitle = 'pendigits';
    classColumn = 17;
    reducedParamsQty = 2;
    
    close;close;

    samples = loadSamples(datasetTitle);

    title1 = char(['Eigendigits de ', datasetTitle]);
    title2 = char(['Scree Plot de ', datasetTitle]);
    [reducedData, eVector, eValues, E] = plotEigenDigits(title1, samples, classColumn, reducedParamsQty); 
    screePlot(title2, eValues);
    plotDecomposition(samples, E, reducedParamsQty);
    
end


function samples = loadSamples(datasetTitle)

    clear samples;
    matFile = char([datasetTitle, '.mat']);
    arffFile = char([datasetTitle, '.arff']);

    if(~exist('samples'))
       if(exist(matFile))
           load(matFile);
       else if(exist(arffFile))
               samples = arff2Mat(arffFile);
           else
               error('Nenhum arquivo .mat ou .arff encontrado.');
           end
       end
    end
end
