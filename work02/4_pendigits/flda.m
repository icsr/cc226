function [reducedData, eVectors, eValues, overallMean] = flda(samples, classColumn, reducedParamsQty)
    
    data = samples;
    data(:, classColumn) = [];
    classes = samples(:, classColumn);

    [Sb Sw overallMean] = scatterMatrix(samples, classColumn);

    [V, D] = eig(Sb, Sw);
    
    eValues = diag(D);
    w = V(:, 1 : reducedParamsQty);
    
    reducedData = data * w;
    
    eVectors = V;
    reducedData = [reducedData classes]; 
    
end