function plotGroups(samples, coord1, coord2, classColumn)
    import java.util.*; 
    
    g = getGroups(samples, classColumn);
    ks = g.keySet();
    it = ks.iterator;
    clf;
    hold on;
    
    i = 1;
    while(it.hasNext())
        k = it.next();
        v = g.get(k);
        classes(i) = k;
        i = i+1;
        ph = plot(v(:,coord1), v(:,coord2), strcat('o'));
        ph.Color = ['[' num2str(color(k,1)) ' ' num2str(color(k,2)) ' ' num2str(color(k,3)) ']'];
        set(ph, 'MarkerEdgeColor','none','MarkerFaceColor', ph.Color);
    end
    
    legend(int2str(classes'),'Location','BestOutside');    
    hold off;

end


function d = color(class, order)
    d = ((hashCode([class, 'seed'])) * order)^(order*5);
    while(d > 1)
        d = d/10;
    end 
end