function samples = arff2Mat(arff)
    samples = weka2matlab(loadARFF(arff));
    s = strsplit(arff, '.');
    save(char(strcat(s(1), '.mat')),'samples'); 
end