function errRMS = calcErrRMS(estimatedY, Y, B, lambda)
    
    ESS = 0;
    TSS = 0;
    mi = mean(Y);
    n = length(Y);
    m = length(B);
        
    for(i = 1 : n)
        ESS = ESS + (estimatedY(i) - Y(i))^2;
        TSS = TSS + (Y(i) - mi)^2;
    end

    Radj = abs(sqrt( 1 - (n-1)/(n-(m+1)) * ESS/TSS));
    
    if(~exist('lambda'))
        lambda = 0;
    end
    
    errRMS = lambda/2 * B'*B;
    for(i = 1 : n)
        errRMS = errRMS + 0.5 * (estimatedY(i) - Y(i))^2;
    end
    
    errRMS = sqrt(2*errRMS / n);
      
end