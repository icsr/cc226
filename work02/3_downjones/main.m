function main()
    import java.util.*;
    stocks = java.util.HashMap;
    format long;
  
    datasetTitle = 'dj30';    
    closePriceIndex = 5;
    
    lambda = 1;
    degrees = [5 7];
    
    load(char(['dj30' '.mat']));
    close; close;
    
    if(~exist('stocks'))
        preparerawdata(datasetTitle);
        load(char(['dj30' '.mat']));
    end
    
    plotRegressionCurves(stocks, lambda, degrees);    
      
end