function preparerawdata(datasetTitle)
    import java.util.*;
    stocks = java.util.HashMap;
    
    [data, dates, ticker] = init(datasetTitle);    
    
    ids = unique(ticker);
    
    sp = 1;
    for(i = 1 : length(ids))
        
        k = findIndexes(ticker, ids(i), sp);
        sp = max(k);
        value = [dates(k) cell2mat(data(k, closePriceIndex))];
        stocks.put(ids(i), value);
                
    end
    
    save(char([datasetTitle '.mat']), 'stocks');            

end

function indexes = findIndexes(cells, value, startPoint)

    startPoint
    k = 1;

    found = false;
    for(i = 1 : length(cells))
        try        
            if(any(strjoin(cells(i)) ~= strjoin(value)))
                if(found)
                    break;
                else
                    continue;
                end
            end
        catch e
            if(found)
                break;
            else
                continue;
            end
        end
        
        found = true;
        indexes(k) = i;
        k = k+1;
        
    end

end



function [data, dates, ticker] = init(datasetTitle)       

    M = loadrawdata;
    ticker = M(:,9);
    dates = datenum(cell2mat(M(:,2)));
    data = M(:, [1 3 4 5 6 7 8]);    
    
    save(char([datasetTitle '.mat']));
end