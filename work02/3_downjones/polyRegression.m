function [estimatedY, errRMS, B] = polyRegression(X, Y, lambda, degree)
       
    X = repPol(X, degree);
    
   [lines col] = size(X);
    
    GAMA = eye(col);
    
    B = inv(X' * X + lambda * GAMA'*GAMA) * X' * Y;
        
    estimatedY = X * B;
    errRMS = calcErrRMS(estimatedY, Y, B);    
    
end