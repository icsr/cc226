function plotRegressionCurves(stocks, lambda, degrees)
    import java.util.*;
    
    close; close;
    
    for(i = 1 : length(degrees))
        plotCurves(char(['Regress�o Polinomial em DJ30 com grau ', num2str(degrees(i))]), stocks, lambda, degrees(i));
    end

end

function plotCurves(label, stocks, lambda, degree)
    import java.util.*;

    lines = 6;
    columns = 5;
    
    ks = stocks.keySet();
    it = ks.iterator();

     elm = 1;
    figure('units', 'normalized', 'outerposition', [0 0 1 1]);
    while(it.hasNext())
        k = it.next();
        
        data = stocks.get(k);
        
        x = data(:,1);
        y = data(:,2);
       
        minDate = min(x);
        x = x  - minDate * ones(size(x));
        
        [estimatedY, errRMS, B] = polyRegression(x, y, lambda, degree);
        
        h = subplot(lines, columns, elm);
        elm = elm + 1;
        hold on
        plot(x, estimatedY, 'r.');
        plot(x, y, 'b.');
        grid on
                   
        str = java.lang.StringBuilder;
        str.append(Arrays.toString(k));
        str.append(' (US$)');
        
        xlabel(['Dias desde ' datestr(minDate)]);
        
        ylabel({char(str.toString()); ['E_{RMS}=' num2str(errRMS, 2)]});
        
    end
    
    subtitle(label);

end

function [ax,h]=subtitle(text)
    ax=axes('Units','Normal','Position',[.075 .10 .85 .85],'Visible','off');
    set(get(ax,'Title'),'Visible','on');
    title(text);
    if (nargout >= 2)
        h=get(ax,'Title');
    end    
end
    
%     figure('units', 'normalized', 'outerposition', [0.5 0 0.5 1]);
%     
%     [lines columns] = size(x);
%    
%     samples = [x y];
%     [estimatedY, errRMS, B] = polyRegression(x, y, lambda, degree);
%     regSamples = [x estimatedY];
% 
%     hold on
%     plot(x(:,1), estimatedY, 'r.');
%     plot(x(:,1), y, 'b.');
% 
%     
%      for(i = 1 : columns)
%         minX(i) = min(x(:, i));
%         maxX(i) = max(x(:, i));
%         
%         delta = (maxX(i) - minX(i)) / pointsQty;
%         c = (y(length(y)) - y(1))/(x(length(x), i) - x(1, i));
%         
%         if(c > 0)
%             eX(:, i) = minX(i) : delta : maxX(i);
%         else
%             eX(:, i) = maxX(i) : -delta : minX(i);
%         end       
%         
%      end
% 
%      eX = repPol(eX, degree);
%     
%      eY = eX * B;    
%      
%      plot(eX(:,1), eY, 'k-');
% 
% %         
% %     for(i = 1 : columns)
% %         subplot(columns, 1, i);
% %         hold on;
% %         
% %         regSamples = sortrows(regSamples, i);        
% %         plot(regSamples(:, i), regSamples(:, columns+1), 'ro');
% %                 
% %         plot(samples(:, i), samples(:, columns+1), 'bo');
% %         plot(fitSamples(:, i), fitSamples(:, columns+1), 'g-');
% %         
% %      
% %         xlabel(labels(i+1, :));
% %         ylabel(labels(1, :));
% %         legend('Estimado', 'Verdadeiro', 'Fit Curve');
% %         hold off;
% %     end
%     
%     %subtitle(char(['Regress�o com Erro RMS de ' num2str(errRMS)]));
%             
% end
% 
%