function XPOL = repPol(X, degree)

    XPOL = [];  
    for(d = 0 : degree)
        XPOL = [XPOL, X.^d];
    end
end