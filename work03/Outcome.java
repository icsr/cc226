import java.io.Serializable;

public class Outcome implements Serializable{
    
    public float errRMS;
    public float[][] contingencyTable;
    public float[] Y;
    public float[] estimatedY;
    public float[] tpr, fpr;
    public Node root;
   
    
    public Outcome(float errRMS, float[][] contingencyTable, float[] Y, float[] estimatedY, Node root, float[] tpr, float[] fpr){
        this.errRMS = errRMS;
        this.contingencyTable = contingencyTable;
        
        this.Y = Y;
        this.estimatedY = estimatedY;
        this.root = root;
       
        this.tpr = tpr;
        this.fpr = fpr;
    }
    
    public String toString(){
     
        StringBuilder str = new StringBuilder();
        
        str.append("Erro RMS: ");
        str.append(errRMS);
        
        str.append("\nTPR: ");
        for(float t : tpr){
            str.append(t);
            str.append("\t");
        }
        
        str.append("\nFPR: ");
        for(float t : fpr){
            str.append(t);
            str.append("\t");
        }
        
        return str.toString();
        
    }
}