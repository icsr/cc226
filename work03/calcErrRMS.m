function errRMS = calcErrRMS(Y, estimatedY)
    n = length(Y);        
    errRMS = 0;
    err2 = (estimatedY - Y).^2;
    
    for(i = 1 : n)
        errRMS = errRMS + 0.5 * err2(i);
    end
    
    errRMS = sqrt(2*errRMS / n);
      
end