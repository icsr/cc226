function e = calcEntropy(samples)

    [m n] = size(samples);
    
    classes = unique(samples(:,n));
    
    e = 0;
    
    for(i = 1 : length(classes))
       p = length(find(samples(:, n) == classes(i)));
       q = p / m;
       e = e - q * log2(q);
    end    
end