function performDecisionTree(node, pValue, purityRatio)

    if(isempty(node))
        return;
    end
    
    [pure actualPurityRatio] = isPure(node, purityRatio);    
    
    if(pure)
        node.splitted = true;
        node.pure = true;
        node.purityRatio = actualPurityRatio;
        disp(['Node ' char(node)]);
        return;
    end

    if(node.isSplitted())
        performDecisionTree(node.left, pValue, purityRatio);
        performDecisionTree(node.right, pValue, purityRatio);
        return;
    end
    
    disp(['Ramificando ', char(node), '...']);
   

    I = infoGain(node, pValue);

    if(isnan(I))
        return;
    end

    node.setAttribute(I(1), I(2));
    
    k0 = find(node.samples(:, node.attributeIndex) <=  node.attributeValue);
    k1 = find(node.samples(:, node.attributeIndex) >  node.attributeValue);
    
    if(isempty(k0) || isempty(k1))
        return;
    end

    node.splitted = true;
    node.left = Node(node, node.samples(k0, :));
    performDecisionTree(node.left, pValue, purityRatio);

    node.right = Node(node, node.samples(k1, :));
    performDecisionTree(node.right, pValue, purityRatio);
           
    

end

function [pure actualPurityRatio] = isPure(node, purityRatio)
   [instances atts] = size(node.samples);
   
   if(atts <= 0 || instances == 0 || instances == 1)
       pure = true;
       actualPurityRatio = 1;
       return;
   end
   
    actualPurityRatio = length(find(node.samples(:, atts) == mode(node.samples(:, atts)))) / instances;    
    pure = actualPurityRatio >= purityRatio;
    
end