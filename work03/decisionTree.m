function root = decisionTree(trainingGroup, purityRatio)
    
    root = Node(true, trainingGroup);
    
    global decisionTreeFile;
                
    [instances atts] = size(trainingGroup);   
    classes = unique(trainingGroup(:, atts));
    
    tic
    for(i = 1 : length(classes))
        performDecisionTree(root, classes(i), purityRatio);
    end
    toc
        
end