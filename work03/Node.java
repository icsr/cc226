import java.util.*;
import java.io.Serializable;

public class Node implements Serializable{
    
    public static int counterId = 0;    
    public int id;
    public Node parent = null;
    public Node left = null;
    public Node right = null;
    public float attributeValue = 1;
    public int attributeIndex = -1;
    public float[][] samples;
    public boolean pure = false;
    public boolean splitted = false;
    public float purityRatio = 0;
  
    
    
    public Node(Node parent, float[][] samples){
        this(parent == null ? true : false, samples);
        this.parent = parent;        
    }
    
    public Node(boolean isRoot, float[][] samples){
        if(isRoot)
            counterId = 0;
        
        this.id = counterId++;
        this.samples = samples;
    }
    
    public void setAttribute(int index, float value){
        attributeIndex = index;
        attributeValue = value;
    }
    
    public String toString(){
        StringBuilder str = new StringBuilder();
        str.append(id);
        
        if(samples != null){
            str.append(" (");
            str.append(samples.length);
            str.append(")");
        }
        
        if(isPure()){
            str.append(" - ");
            str.append(100 * purityRatio);
            str.append("% of class " + samples[0][samples[0].length - 1]);
        }else if(!isSplitted()){
            str.append(" - not splitted");            
        }else{
            str.append(String.format(" - split em %d/%f",  attributeIndex, attributeValue));
        }
            
        
        if(parent != null){
            str.append(" - child of " + parent.id);            
        }
            
        return str.toString();
    }   
    
    public boolean isSplitted(){
        return splitted;
    }
    
     public boolean isPure(){         
        return pure;
    }


}