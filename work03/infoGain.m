function I = infoGain(node, pValue)

    [n atts] = size(node.samples);    
    [p nil] = size(find(node.samples(:, atts) == pValue));
    
    h = calcEntropy(node.samples);

    for(i = 1 : atts - 1)
        mn = min(node.samples(:, i));
        mx = max(node.samples(:, i));
        stepJ = linspace(mn, mx);
        
        I(i,1) = -1;
        I(i,2) = -1;
        for(j = stepJ)
            kL = find(node.samples(:, i) <= j);
            kR = find(node.samples(:, i) > j);
            
            if(isempty(kL) || isempty(kR))                
                continue;
            end
    
            hi = h - length(kL) / n * calcEntropy(node.samples(kL,:)) - length(kR) / n * calcEntropy(node.samples(kR,:));
            if(hi > I(i,2))
                I(i,1) = j;
                I(i,2) = hi;
            end

        end

    end

    [lins cols] = size(I);
    if(lins > 1)
        k = find(I(:,2) == max(I(:,2)));
        I = I(k,:);
    end
    
    I = [k I];
    if(I(1,3) == 0)
        I = NaN;
    end
    
    [lins cols] = size(I);
    if(lins > 1)
        I = I(1,:);
    end
    
end