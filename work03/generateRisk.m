function samples = generateRisk(samples)

    g = 2; %sexo
    wg = 4; %peso
    h = 5; %estatura
    ws = 6; %cintura
    class = 10;

    imc = samples(:,wg) ./ pow2(samples(:, h)/100);

    for(i = 1 : length(imc))
        gender = samples(i, g);
        weight = samples(i, wg);
        height = samples(i, h);
        waist = samples(i, ws);
        
        
        if((gender == 1 & waist <= 102) | (gender == 2 & waist <= 88))            
            if(imc(i) < 18.5)
                risk(i) = 1;
            end
            
            if(imc(i) >= 18.5 & imc(i) < 25)
                risk(i) = 1;
            end
            
            if(imc(i) >= 25 & imc(i) < 30)
                risk(i) = 2;
            end
            
            if(imc(i) >= 30 & imc(i) < 35)
                risk(i) = 3;
            end
            
            if(imc(i) >= 35 & imc(i) < 40)
                risk(i) = 4;
            end
            
            if(imc(i) >= 40)
                risk(i) = 5;
            end
        else
            if(imc(i) < 18.5)
                risk(i) = 1;
            end
            
            if(imc(i) >= 18.5 & imc(i) < 25)
                risk(i) = 1;
            end
            
            if(imc(i) >= 25 & imc(i) < 30)
                risk(i) = 3;
            end
            
            if(imc(i) >= 30 & imc(i) < 35)
                risk(i) = 4;
            end
            
            if(imc(i) >= 35 & imc(i) < 40)
                risk(i) = 4;
            end
            
            if(imc(i) >= 40)
                risk(i) = 5;
            end            
        end
    end


    samples(:, [class]) = [];
    samples = [samples risk'];

end