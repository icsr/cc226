function groups = separateGroups(samples, qtyOfGroups)
    groups = java.util.HashMap;
    
    n = round(length(samples)/qtyOfGroups);
    
    
    
    for(i = 1 : qtyOfGroups)
        
        final = n * i;
        if(i == qtyOfGroups)
            final = length(samples);
        end
                
        groups.put(i, samples( n * (i-1) + 1 : final , :));
                
    end
    
end