import java.lang.StringBuilder;
import java.lang.String;

public class Printer{
    
    static char prefixChar = '-';
    
    public static void print(Node node){
        System.out.println(toString(node));
    }
    
    public static String toString(Node node){
        
        StringBuilder lines = new StringBuilder();        
        toString(node, lines);
        
        return lines.toString();
        
    }
    
    public static void toString(Node node, StringBuilder lines){
        
        if(node == null){
            return;
        }
 
        int height = 0;
        Node parent = node.parent;
        StringBuilder prefix = new StringBuilder();
        
        while(parent != null){
            prefix.append(prefixChar);
            height++;
            parent = parent.parent;
        }
        
        
        StringBuilder nodeDescription = new StringBuilder();
        nodeDescription.append(node.id);
        
       if(node.parent != null){
            nodeDescription.append(" filho de ").append(node.parent.id);
            int index = node.parent.attributeIndex;
            float value = node.parent.attributeValue;
            boolean right = node.parent.right == node;
            nodeDescription.append(": ").append("x").append(index).append(right ? " >  " : " <= ").append(value);
        }
        
        nodeDescription.append(" - ").append(node.samples.length).append(" amostras");
        
         if(node.isPure()){
            int intClassColumn = node.samples[0].length - 1;
            nodeDescription.append(" ").append(100 * node.purityRatio).append("% of class ").append(node.samples[0][intClassColumn]);
        }
        
        lines.append(prefix.toString()).append(nodeDescription.toString()).append("\n");
        
        toString(node.left, lines);
        toString(node.right, lines);
        
    }

}





