function main(reprocess, risk, purityRatio)

    trainingDataset = 'dataset.mat';   
    outcomeFile = 'outcome.mat';

    clc;       
    javaclasspath('./');
    import java.util.*;   
    format short;
    
    load(trainingDataset);
    [m n] = size(samples);
    trainingSamples = samples(1:15000, :);
    testSamples = samples(15001:m, :);
    clear samples;
    
    if(~exist('risk'))
        risk = false;
    end
    
    if(risk)
        trainingSamples = generateRisk(trainingSamples);
        testSamples = generateRisk(testSamples);
    end
    
    if(~exist('reprocess'))
        reprocess = false;
    end
    
    if(~exist('purityRatio'))
        purityRatio = 1;
    end
    
    if(~exist(outcomeFile))
        reprocess = true;
    end
    
    
%     trainingSamples = [5, 5, 1; 2, 2, 1; 1, 1, 1; 2, 3, 1; 3, 2, 1; 4, 5, 1; 5, 10, 2; 3, 4, 1; 4, 2, 1; 3, 4, 1; 1, 1, 1; 6, 4, 2; 8, 4, 2; 6, 6, 2; 6, 8, 2; 11, 7, 2; 6, 10, 2; 6, 8, 2; 7, 8, 2; 1, 10, 1; 3, 8, 1; 4, 7, 1; 3, 8, 1; 8, 5, 2; 6, 6, 2; 3, 3, 1; 7, 2, 3; 8, 5, 2; 7, 7, 2; 9, 9, 2];                 
%     testSamples = [7 2 3; 5 5 1; 4 4 1; 7 7 2; 5 5 1; 5 4 1; 3 3 1; 9 2 3];
        
                   
    if(reprocess)
        root = decisionTree(trainingSamples, purityRatio);
    else
        load(outcomeFile);        
        root = outcome.root;
    end
      
        
    outcome = classifyWithDecistionTree(root, testSamples);
    tree = Printer.toString(root);
    
    save(outcomeFile, 'outcome', 'tree');
    disp(outcome);
    disp(outcome.contingencyTable);
    disp(['Resultados salvos em ' outcomeFile]);
       
end