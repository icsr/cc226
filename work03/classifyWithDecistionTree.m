function outcome = classifyWithDecistionTree(root, testGroup)

    [Y estimatedY] = estimate(root, testGroup);   
        
    errRMS = calcErrRMS(Y, estimatedY);
    contingencyTable = calcContingencyTable(Y, estimatedY);

    [m n] = size(contingencyTable);        
    for(i = 1 : m)
        tpr(i) = contingencyTable(i,i) / sum(contingencyTable(i,:)) *100;
        
        aux = contingencyTable;
        aux(i,:) = [];
                
        num = sum(aux(:, i));
        aux(:,i) = [];
        fpr(i) = sum(num) / (sum(num) + sum(sum(aux))) * 100;
    end


    outcome = Outcome(errRMS, contingencyTable, Y, estimatedY, root, tpr, fpr);

end

function [Y estimatedY] = estimate(root, samples)

    [instances atts] = size(samples);
    for(i = 1 : instances)
       
        estimatedY(i) = perform(samples(i,:), root);
        Y(i) = samples(i, atts);
        
    end
    
    Y = Y';
    estimatedY = estimatedY';
    
end

function predictedClass = perform(testSample, node)
        
    [instances atts] = size(node.samples);
    if(node.isPure())        
        predictedClass = mode(node.samples(:, atts));
        
        if(predictedClass ~= testSample(atts))        
            disp(['>> Discrepância em ' char(node) ' - real: ' num2str(testSample(atts)) ' - estimado: ' num2str(predictedClass)]);            
        end
        
        return;
    end
    
    index = node.attributeIndex;
    value = node.attributeValue;
    
    if(testSample(index) <= value & ~isempty(node.left))
        predictedClass = perform(testSample, node.left);
        return;
    end
    
    if(~isempty(node.right))
        predictedClass = perform(testSample, node.right);
        return;
    end
    
    predictedClass = round(mean(node.samples(:, atts)));    

end

function contingencyTable = calcContingencyTable(Y, estimatedY)
   
    est = [Y, estimatedY];
    
    classes = unique(Y);
    
    for(i = 1 : length(classes))
        for(j = 1 : length(classes))
            contingencyTable(i,j) = length(find((est(:,1) == classes(i) & est(:,2) == classes(j))));
        end
        
    end
    
end