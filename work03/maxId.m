function m = maxId(node)
    import Node;        
    global m;    
    m = -1;
    perform(node);
end

function m = perform(node)

    global m;

    if(isempty(m))
        m = -1;
    end
    
    if(isempty(node))
        return;
    end
       
    m = max(m, node.id);
    
    if(~isempty(node.left))
        m = max(m, maxId(node.left));
    end
    
    if(~isempty(node.right))
        m = max(m, maxId(node.right));
    end


end