function sampleGroups = getSampleGroups(groups, testGroupKey)

    ks = groups.keySet();
    it = ks.iterator();
    sampleGroups = [];
            
    while(it.hasNext)
       k = it.next();
       if(k == testGroupKey)
           continue;
       end
       sg = groups.get(k);       
       
       sampleGroups = [sampleGroups; sg];
       

    end        

end