function plotErrorRMS(errorsRMS)

    errorsRMS

    figure('units', 'normalized', 'outerposition', [0 0 0.5 1]);
    
    x = 1 : length(errorsRMS);
    bar(x, errorsRMS);
    grid on
    title('Predi��o de TACF com Regress�o Linear M�ltipla');
    xlabel('10-fold cross-validation');  
    ylabel('Erro RMS');
   
end